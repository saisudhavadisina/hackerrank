import itertools as it

string = "ball palm heat listen post enlist silent stop lamp"

def sign(string: str) -> str:
    return ''.join(sorted(string))
print(sign(string))

def get_anagrams(s):
    sorted_words = sorted(s.split(), key = sign)
    clusters = [tuple(b) for a, b in it.groupby(sorted_words, sign)]
    return [k for k in clusters if len(k) > 1]

print(get_anagrams(s))