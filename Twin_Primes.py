num1 = int(input(""))
num2 = int(input(""))
def is_prime(num):
    pcount = 0
    for i in range(2, num):
        if num % i == 0:
            pcount += 1
    if pcount == 0:
        return True
    else:
        return False
def twinprime(num1, num2):
    if is_prime(num1) and is_prime(num2) and (num2 - num1) == 2 or (num1 - num2) == 2:
        return True
    else:
        return False
if twinprime(num1, num2):
    print("True")
else:
    print("False")
