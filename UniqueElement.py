import os
import sys

def unique(ar):
    lst = []
    for i in ar:
        if ar.count(i) == 1:
            lst.append(i)
            
    return ' '.join([str(elem) for elem in lst]) 

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')
    
    ar_count = int(input())
    
    ar = list(map(int, input().rstrip().split()))
    
    result = unique(ar)
    
    fptr.write(result + '\n')
    
    fptr.close()
