import os
import sys

def removeDuplicates(ar):
    
    lst = []
    lst1 = [set(i for i in ar)]
    for i in ar:
        if ar.count(i) != 1:
            lst.append(i)
            
    return ' '.join([str(elem) for elem in lst]) 

if __name__ == '__main__':
    ar_count = int(input())
    
    ar = list(map(int, input().rstrip().split()))
    
    result = removeDuplicates(ar)
    print(str(result) + '\n')
