import os
import sys
# Complete the timeConversion function below.

def timeConversion(s):
    return str(12 + int(s[:2])) + s[2:8] if s[:7:-1] == "MP" else s[:8]

if __name__ == '__main__':
    s = input()
    print(timeConversion(s))
