#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int countTrailingzeroes(long long int n){
    int trailingZeroes = 0;
    for (int i = 5; n / i >= 1; i *= 5) {
        trailingZeroes += n / i;
        if(i > n / i)
            break;
    }
    return trailingZeroes;
}
int main() {
    int t;
    long long int n;
    scanf("%d", &t);
    for(int i = 0; i < t; i++){
        scanf("%lld", &n);
        printf("%d\n", countTrailingzeroes(n));
    }
    return 0;
}

