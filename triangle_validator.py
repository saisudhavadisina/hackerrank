def triangle(a, b, c):
    if (1 <= a, b, c <= pow(10, 9)):
        if ((a + b >= c) or (a + c >= b) or (b + c >= a)) and ((a - b <= c) or (a - c <= b) or (b - c <= a) or (b - a <= c) or (c - a <= b) or (c - b <= a)):
            return "Yes"
        return "No"
    
if __name__ == '__main__':
    
    x, y, z = input().split()
    a, b, c = [int(i) for i in [x, y, z]]
    
    result = triangle(a, b, c)
    print(result)
