def utopianTree(n):
    res = 0
    for i in range(n + 1) :
        res = res + 1 if i % 2 == 0 else res * 2
    return res

if __name__ == '__main__':
    t = int(input())
    for i in range(t):
        n = int(input())
        result = utopianTree(n)
        print(str(result) + '\n')